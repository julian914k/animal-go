﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MoveDirect {Left, Forward, Right}

public class MoveBtn : Button {
	public MoveDirect direct;
	void Update() {
		try {
			switch (direct) {
			case MoveDirect.Left:
				CharacterMove.instance.leftDown = IsPressed ();
				break;
			case MoveDirect.Right:
				CharacterMove.instance.rightDown = IsPressed ();
				break;
			}
		} catch (System.Exception ex) {
			
		}

	}
}
