﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMove : MonoBehaviour {
	public float moveTime;
	public Vector3 moveV;
	Vector3 startPosV;
	Rigidbody rb;
	void Start() {
		startPosV = transform.position;
		rb = GetComponent<Rigidbody> ();
//		rb.velocity = moveV;
		StartCoroutine (Move ());
	}
	void Update() {
		rb.AddForce (moveV);
	}
	IEnumerator Move() {
		while (true) {
			yield return new WaitForSeconds (moveTime);
			transform.position = startPosV;
		}
	}
}
