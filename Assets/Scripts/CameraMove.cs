﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour, IListener
{
    public static CameraMove instance;
    Animator anim;
    public Transform targetTf;
    Vector3 dV;
    void Awake()
    {
        instance = this;
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        dV = transform.position - targetTf.position;
        EventManager.Instance.AddListener(EVENT_TYPE.Finish, this);
    }
    void LateUpdate()
    {
        //		Vector3 tV = targetTf.position + dV;
        //		tV.y = transform.position.y;
        //		transform.position = tV;
        transform.position = targetTf.position + dV;

        //		Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, CharacterMove.instance.targetZoomF, 0.1f);
    }
    public void OnEvent(EVENT_TYPE eventType, Component sender, object param = null)
    {
        switch (eventType)
        {
            case EVENT_TYPE.Finish:
                anim.SetTrigger("Finish");
                break;
            default:
                break;
        }
    }
}
