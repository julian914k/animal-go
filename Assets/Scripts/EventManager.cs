﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {
	private static EventManager instance;
	public static EventManager Instance {
		get {return instance;}
	}
	void Awake() {
		if (instance) {
			DestroyImmediate (gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad (gameObject);
	}

	private Dictionary <EVENT_TYPE, List<IListener>> Listeners = new Dictionary<EVENT_TYPE, List<IListener>> ();

	public void AddListener(EVENT_TYPE eventType, IListener listener) {
		List <IListener> ListenList = null;
		if (Listeners.TryGetValue (eventType, out ListenList)) {
			ListenList.Add (listener);
			return;
		}
		ListenList = new List<IListener> ();
		ListenList.Add (listener);
		Listeners.Add (eventType, ListenList);
	}
	public void EventPost(EVENT_TYPE eventType, Component sender, object param = null)
	{
		List<IListener> ListenList = null;
		if (!Listeners.TryGetValue (eventType, out ListenList))
			return;
		foreach (IListener listener in ListenList) {
			if (!listener.Equals (null))
				listener.OnEvent (eventType, sender, param);
		}
	}
	public void RemoveEvent(EVENT_TYPE eventType) {
		Listeners.Remove (eventType);
	}
	public void Initialize() {
		Dictionary <EVENT_TYPE, List<IListener>> tempListeners = new Dictionary<EVENT_TYPE, List<IListener>> ();
		foreach (KeyValuePair<EVENT_TYPE, List<IListener>> Item in Listeners) {
			for (int i = Item.Value.Count - 1; i >= 0; i--) {
				if (Item.Value [i].Equals (null))
					Item.Value.RemoveAt (i);
			}
			if (Item.Value.Count > 0)
				tempListeners.Add (Item.Key, Item.Value);
		}
		Listeners = tempListeners;
	}
}
