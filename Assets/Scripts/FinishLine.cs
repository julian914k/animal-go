﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour {
	void OnTriggerEnter(Collider col) {
		if (col.transform.tag.Equals ("Player")) {
			EventManager.Instance.EventPost (EVENT_TYPE.Finish, this);
		}
	}
}
