﻿using UnityEngine;
using System.Collections;

public enum EVENT_TYPE {Finish};

public interface IListener {
	void OnEvent(EVENT_TYPE eventType, Component sender, object param = null);
}
