﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EatableObj : MonoBehaviour {
	void OnTriggerEnter(Collider col) {
		StageManager.instance.Coin++;
		AudioManager.instance.coinAs.Play ();
		Destroy (gameObject);
	}
}
