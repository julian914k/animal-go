﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour {
	public Vector3 directV;
	public float speed = 1f;
	void Update () {
		transform.Rotate (directV * speed);
	}
	void OnTriggerEnter2D(Collider2D col) {
		
	}
}
