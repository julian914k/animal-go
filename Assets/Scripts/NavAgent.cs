﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavAgent : MonoBehaviour {
	public Vector3 targetPos;
	public GameObject targetObj;
	NavMeshAgent agent;
	Animator anim;
	// Use this for initialization
	void Awake () {
		agent = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
	}
	void Update() {
		agent.SetDestination (targetPos);
		anim.SetFloat ("Speed_f", agent.velocity.magnitude * 0.1f);
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("Down!");
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
				agent.speed = 3.5f;
				targetPos = hit.point;
				targetObj.transform.position = hit.point;
			}
		}
		if (Input.GetKey(KeyCode.LeftShift)) {
			agent.speed = 7f;
		} else {
			agent.speed = 3.5f;
		}
	}
	void OnCollisionEnter(Collision col) {
		if (col.transform.GetComponent<VehicleMove> ()) {
			Destroy (col.transform.GetComponent<VehicleMove> ());
			anim.SetBool ("Death_b", true);
		}
	}
}
