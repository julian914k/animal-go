﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {
	void OnTriggerEnter(Collider col)
	{
		if (col.transform.tag.Equals ("Player")) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("Play2");
		}
	}
}
