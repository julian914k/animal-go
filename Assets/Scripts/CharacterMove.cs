﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour {


	public static CharacterMove instance;
	public float speed = 1f;
	public MoveDirect direct = MoveDirect.Forward;
	public Vector3 targetDirectV = Vector3.forward;

	CharacterController cont;
	Animator anim;
//	Rigidbody rbody;
	void Awake() {
		instance = this;
		cont = GetComponent<CharacterController> ();
		anim = GetComponent<Animator> ();
//		rbody = GetComponent<Rigidbody> ();
	}
	void Start() {
		StageManager.instance.startPosV = transform.position;
	}
	public bool leftDown = false, rightDown = false, jumpTrigger = false;
	void Update() {
		if (leftDown)
			targetDirectV = Vector3.forward + Vector3.left;
		else if (rightDown)
			targetDirectV = Vector3.forward + Vector3.right;
		else
			targetDirectV = Vector3.forward;

		StageManager.instance.CurrentPosV = transform.position;
	}
	public Vector3 baseGravityScale = Vector3.down;
	Vector3 gravityV = Vector3.zero;
	bool canJump = false, isFinish = false, isEnd = false;
	void FixedUpdate() {
		if (isEnd)
			return;
		Vector3 moveV = Vector3.zero;
		switch (StageManager.instance.gameState) {
		case GameState.Play:
			transform.forward = Vector3.Lerp (transform.forward, targetDirectV, 0.15f);
			moveV = transform.forward * speed;
			break;
		case GameState.Finish:
			if (isFinish) {
				Finish ();
				isEnd = true;
			} else {
				transform.forward = StageManager.instance.goalTf.position - transform.position;
				moveV = transform.forward * speed;
			}
			break;
		default:
			break;
		}



		if (!cont.isGrounded) {
			gravityV += baseGravityScale;
		} else {
			if (canJump) {
				gravityV = Vector3.zero;
			}
//			if (jumpTrigger) {
//				gravityV.y = jumpPower;
//				jumpTrigger = false;
//			}
		}
		if (cont.isGrounded) {
			canJump = true;
		}
		moveV += gravityV;

		cont.Move (moveV);
//		Vector3 veloV = transform.forward * speed;
//		veloV.y = rbody.velocity.y;
//		rbody.velocity = veloV;
//		rbody.AddForce (transform.forward * speed);

	}
	public float targetZoomF = 24f;
	public float jumpPower = 300f;
	public void JumpBtn() {
		if (canJump) {
			gravityV.y = jumpPower;
			canJump = false;
//			jumpTrigger = false;
		}
//		if (!jumpTrigger)
//			jumpTrigger = true;
//		rbody.AddForce (Vector3.up * jumpPower);
//		StartCoroutine (Jump_Zoom ());
	}
	IEnumerator Jump_Zoom() {
		targetZoomF = 35f;
		yield return new WaitForSeconds (1f);
		targetZoomF = 24f;
	}
	public void Finish() {
		anim.SetFloat ("Speed_f", 0f);
		anim.SetBool ("Eat_b", true);
		anim.SetTrigger ("Eat_t");
	}
	void OnTriggerEnter(Collider col) {
		if (col.transform.tag.Equals ("Goal")) {
			isFinish = true;
		}
	}
}
