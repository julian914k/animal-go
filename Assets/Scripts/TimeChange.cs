﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeChange : MonoBehaviour {
	public RawImage rImage;
	public string url = "http://incoding.kr/resource/unity_pp2.png";

	void Start() {
		StartCoroutine (GetTexture (url));
	}
	IEnumerator GetTexture(string _url) {
		WWW www = new WWW (_url);
		yield return www;
		rImage.texture = www.texture;
	}

}
