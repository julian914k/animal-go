﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState { Play, Pause, End, Finish }

public class StageManager : MonoBehaviour, IListener
{
    public static StageManager instance;
    public Text coinText, distanceText;
    public Vector3 startPosV;
    Vector3 currentPosV = Vector3.zero;
    public GameState gameState = GameState.Play;
    public Transform goalTf;
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Coin = Coin;
        EventManager.Instance.AddListener(EVENT_TYPE.Finish, this);
    }
    public void OnEvent(EVENT_TYPE eventType, Component sender, object param = null)
    {
        switch (eventType)
        {
            case EVENT_TYPE.Finish:
                gameState = GameState.Finish;
                break;
            default:
                break;
        }
    }
    public Vector3 CurrentPosV
    {
        get { return currentPosV; }
        set
        {
            currentPosV = value;
            float totalDistance = (goalTf.position - startPosV).magnitude;
            float currentDistance = (value - startPosV).magnitude;
            //CurrentPosV			Debug.Log (totalDistance + ", " + currentDistance);
            distanceText.text = (int)(currentDistance / totalDistance * 100f) + "%";
        }
    }
    public int Coin
    {
        get { return PlayerPrefs.GetInt("SaveCoin", 0); }
        set
        {
            coinText.text = value.ToString("N0");
            PlayerPrefs.SetInt("SaveCoin", value);
        }

    }
}
